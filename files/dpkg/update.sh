#!/bin/bash

telegraf_version=telegraf_1.11.5-1_amd64.deb
influxdb_version=influxdb_1.7.8_amd64.deb
grafana_version=grafana_6.3.5_amd64.deb

wget https://dl.influxdata.com/telegraf/releases/$telegraf_version
wget https://dl.influxdata.com/influxdb/releases/$influxdb_version
wget https://dl.grafana.com/oss/release/$grafana_version

mv $telegraf_version telegraf-latest.deb
mv $influxdb_version influxdb-latest.deb
mv $grafana_version grafana-latest.deb
